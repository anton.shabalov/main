global _start
%define SYS_READ  0
%define SYS_WRITE 1
%define SYS_EXIT  60
%define STDIN     0
%define STDOUT    1

%define EXIT_OK   0

%define INT_BUFFER_SIZE 100


section .text

exit:
  mov     rax, SYS_EXIT
  mov     rdi, EXIT_OK
  syscall

  ;; rdi = addr
  ;; returns:
  ;; rax = length
string_length:
  xor rax, rax
.loop:
  mov dl, [rdi + rax]
  test dl, dl
  jz .stop
  inc rax
  jmp .loop
.stop:
  ret

  ;; rdi = string
string_print:
  push rdi
  call string_length
  mov rdx, rax
  pop rsi
  mov rax, SYS_WRITE
  mov rdi, STDOUT
  syscall
  ret

  ;; rdi = buffer, rsi = length
  ;; returns:
  ;; rax = length of read
string_read:
  push rdi
  dec rsi
  mov rax, SYS_READ
  mov rdx, rsi
  mov rsi, rdi
  mov rdi, STDIN
  syscall
  pop rdi
  mov byte[rdi + rax], 0
  ret

  ;; rdi = number
int_print:
  push rbp
  mov rbp, rsp
  push qword 0
  mov rcx, rsp
  sub rsp, INT_BUFFER_SIZE-8
  mov rax, rdi
  mov r9, 10

.loop:
  xor rdx, rdx
  div r9
  add dl, '0'
  mov [rcx], dl
  dec rcx
  test rax, rax
  jnz .loop

  inc rcx
  mov rdi, rcx
  call string_print

  add rsp, INT_BUFFER_SIZE
  pop rbp
  ret

  ;; rdi = buffer
int_parse:
    xor r8, r8
    xor rcx, rcx
    mov r10, 10
.A: mov r8b, [rdi]
    inc rdi
    cmp r8b, '0'
    jb .NO
    cmp r8b, '9'
    ja .NO
    xor rax, rax
    sub r8b, '0'
    mov al, r8b
    inc rcx
.B: mov r8b, [rdi]
    inc rdi
    cmp r8b, '0'
    jb .OK
    cmp r8b, '9'
    ja .OK
    inc rcx
    mul r10
    sub r8b, '0'
    add rax, r8
    jmp .B
.OK:
.NO:mov rdx, rcx
    ret























































section .data
number: db "12345", 0
section .text
_start:

  mov rdi, number
  call int_parse
  mov rdi, rax
  call int_print
  call exit
